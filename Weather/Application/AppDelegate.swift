//
//  AppDelegate.swift
//  Weather
//
//  Created by Dmitriy Demchenko on 1/27/17.
//  Copyright © 2017 Dmitriy Demchenko. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  var window: UIWindow?
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    // Override point for customization after application launch.
    return true
  }
  
}
